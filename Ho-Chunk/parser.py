#!/usr/bin/env python
# -*- coding: utf-8 -*-

import textReader

fileLines = textReader.GetFileLines( "plaintext.txt" )

csvHeader = [ "Hočąk", "English", "Sources" ] #, "Classifications" ]
csvLines = []

sourceList = textReader.GetFileLines( "sources.txt" )
sourceDict = {}
sourceKeys = []

# Read sources

for source in sourceList:
    # [ABBREVIATION] Information
    if ( source == "\n" or source == "" ):
        continue
        
    source = source.strip()
    split1 = source.split( "]" )

    key = split1[0].replace( "[", "" )
    value = split1[1]

    sourceDict[ key ] = value
    sourceKeys.append( key )

# HOCAK - ENGLISH [CLASSIFICAITON] [SOURCE]

errors = 0
errorList = []
errorMessages = []

for i in range( len( fileLines ) ):
    line = fileLines[i]
    
    # Remove \n (newline) at end of lines...
    line = line.strip()

    lineError = False
    errorMessage = "Line " + str( i ) + "... "

    # Check for parsing errors
    #if ( "[" not in line ):
    #    errorMessage += "Line mising separator [ \n"
    #    lineError = True
        
    if ( "—" not in line and "–" not in line ):
        errorMessage += "Line mising separator – or — \n"
        lineError = True
        
    else:
        s1 = line.split( "—" )
        if ( len( s1 ) < 2 ):
            s1 = line.split( "–" )                

    if ( lineError ):
        # Log error for analysis
        errors += 1
        errorMessages.append( errorMessage + "\t" + line + "\n" )
        errorList.append( line + "\n" )
        continue
        
    else:
        try:
            # Parse the line
            # Split HOCAK - ENGLISH
            split1 = line.split( "—" )
            if ( len( split1 ) < 2 ):
                split1 = line.split( "–" )
            #if ( len( split1 ) < 2 ):
                #split1 = line.split( "–" )
            # Split ENGLISH [CLASSIFICATION]
            split2 = split1[1].split( "[" )

            hocak = split1[0].strip()
            #english = split2[0].strip()
            english = split1[1].strip()
            #classif = split2[1].replace("]", "").strip()
            
            #csvLines.append( { "Hočąk" : hocak, "English" : english, "Classification" : classif } )

            src = ""

            # This takes a lot longer when including sources.
            for source in sourceKeys:
                if ( source in line ):
                    src = sourceDict[ source ]
                    
            csvLines.append( { "Hočąk" : hocak, "English" : english, "Sources" : src } )
            
        except:
            errors += 1
            errorMessages.append( "Line " + str( i ) + "... Unclassified error with line \n\t" + line + "\n" )
            errorList.append( line + "\n" )

print( "Errors: " + str( errors ) )

with open( "errors.txt", "w" ) as fileHandle:
    fileHandle.write( "ERROR MESSAGES\n" )
    for error in errorMessages:
        fileHandle.write( error + "\n" )
        
    fileHandle.write( "\nMISSING LINES\n" )
    for error in errorList:
        fileHandle.write( error + "\n" )

with open( "hocak-english.csv", "w" ) as fileHandle:
    # Header
    for h in range( len( csvHeader ) ):
        if ( h > 0 ):
            fileHandle.write( "," )
        fileHandle.write( csvHeader[h] )

    fileHandle.write( "\n" )

    # Data
    for data in csvLines:
        fileHandle.write( "\"" + data["Hočąk"] + "\"," )
        fileHandle.write( "\"" + data["English"] + "\"," )
        fileHandle.write( "\"" + data["Sources"] + "\"\n" )
        #fileHandle.write( "\"" + data["English"] + "\"," )
        #fileHandle.write( "\"" + data["Classification"] + "\"\n" )      

print( "File written" )
