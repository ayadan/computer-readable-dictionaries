Ho-Chunk Indian Language

## Sources

Hočąk Lexicon from https://hotcakencyclopedia.com/ho.HocakLexicon.html
for the core text document that was parsed.

## Notes

I wrote the **parser.py** file to parse the plaintext file,
but there contains inconsistencies. These are logged in
**errors.txt** and manually added back to the csv file.

There is an inconsistency in the separator; these different characters are used:
—   –

But also - is used in suffixes/prefixes, so I don't want to split purely on that.

## Additional Resources

* [native-languages.org](http://www.native-languages.org/hochunk.htm)
* [Hočąk Lexicon](https://hotcakencyclopedia.com/ho.HocakLexicon.html)
* [Brief Ho-Chunk Language Vocabularies, 1830-1930](https://www.wisconsinhistory.org/turningpoints/search.asp?id=1653)
